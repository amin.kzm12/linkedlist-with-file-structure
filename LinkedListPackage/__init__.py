from LinkedListPackage.linkedlist import LinkedList
from LinkedListPackage.node import Node
if __name__ == '__main__':
  linked_list = LinkedList(1)
  linked_list.insertNode(2)
  linked_list._appendNode1(4)
  linked_list.insertNode(8)
  linked_list._appendNode1(9)
  newNode = Node(value=22)
  linked_list._insertNode(newNode)
  linked_list.printList()
  print("\n",linked_list.linkedListLen())

