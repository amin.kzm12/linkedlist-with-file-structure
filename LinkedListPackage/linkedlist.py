from .node import Node


class LinkedList:
  def __init__(self, value):
    self.head = Node(value)

  def insertNode(self, value):
    newNode = Node(value, self.head)
    self.head = newNode

  def _insertNode(self, node):
    node.next = self.head
    self.head = node

  def _appendNode1(self, value: int):
    temp_node = self.head
    while (temp_node.next is not None):
      temp_node = temp_node.next
    appended_node = Node(value, None, temp_node)
    temp_node.next = appended_node
    temp_node = appended_node

  def _appendNode2(self, node: Node):
    temp_node = self.head
    while (temp_node.next is not None):
      temp_node = temp_node.next
    temp_node.next = node
    temp_node = node

  def search(self, value):
    temp_list = self.head
    while (temp_list is not None):
      if (value == temp_list.value):
        return True
      temp_list = temp_list.next
    return False

  def deleteFirst(self):
    if (not self.head):
      return None
    temp_node = self.head
    self.head = temp_node.next
    temp_node = None

  def linkedListMerge(self, linklst):
    pass


  def linkedListLen(self):
    len=0
    tempNode = self.head
    while tempNode is not None:
      len+=1
      tempNode=tempNode.next
    return len

  def getMiddle(self):
    pass

  def printList(self):
    iterationList = self.head
    arrow = "->"
    while (iterationList):
      if iterationList.next is None:
        arrow = ""
      print(iterationList.value, arrow, sep='', end='', flush=False)
      iterationList = iterationList.next
